# In-time - danger and safety notifications - backend part

The python server behind in-time application, aggregating location information, monitoring danger notices and exposing the key safety information to friends, family or rescue teams.

## install

* you need a mongo server running on your machine, we did it like that
  ```
  docker run --name mongo-srv -p 27017:27017 -d mongo
  ```
* then prepare python environment:
  * you might want to do it in a virualenv: `python3 -mvenv env && source env/bin/activate`
  * install dependencies `pip3 install -r requirements.txt`
  * run the app with flask dev server: `FLASK_APP=intime/intime.py flask run --no-reload `
  * the app listens on `localhost:5000`, frontend should look for it there

