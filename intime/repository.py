from flask_pymongo import PyMongo
from bson import ObjectId
from pymongo.results import InsertOneResult
from pymongo.results import UpdateResult
import json


class Repo:
    in_memory_danger = {}

    def __init__(self, app):
        self.mongo = PyMongo(app)

    def checkin_save(self, checkin):
        return self.mongo.db.checkin.insert_one(checkin).inserted_id

    def checkin_all(self):
        return list(self.mongo.db.checkin.find())

    def register(self, login_data):
        email = login_data['email']
        device = login_data['registration_id']
        result = self.mongo.db.users.update_one({'email':email}, {'$addToSet': {'devices': device}})

        if not result.matched_count:
            if 'devices' not in login_data and 'registration_id' in login_data:
                login_data['devices'] = [login_data['registration_id']]
                del login_data['registration_id']
            result = self.mongo.db.users.insert_one(login_data)

        return self.user(email)

    def add_observer(self, observed, observer):
        self.mongo.db.users.update_one({'email': observed}, {'$addToSet': {'observers': observer}})
        return self.user(observed)

    def users(self):
        return list(self.mongo.db.users.find())

    def user(self, email):
        return self.mongo.db.users.find_one({'email': email})

    def find_user_by_device(self, device):
        return self.mongo.db.users.find_one({'devices': device})

    def disaster_save(self, disaster):
        return self.mongo.db.disasters.insert_one(disaster)

    def disaster_add_people(self, idx, people):
        if idx not in self.in_memory_danger:
            self.in_memory_danger[idx] = []
        self.in_memory_danger.extend(people)
        # return self.mongo.db.disasters.update_one({'_id': idx}, {'$addToSet': {'people', {'$each': [people]}}})

    def disaster_update_person(self, idx, person):
        danger = self.in_memory_danger[idx]
        for pers in danger:
            if pers['email'] == person['email']:
                pers['safe'] = person['safe']

        # return self.mongo.db.disasters.update_one({'_id': idx}, {'$addToSet': {'people', {'$each': [people]}}})

    def disaster_status(self, idx):
        return self.in_memory_danger

    def disasters(self):
        return list(self.mongo.db.disasters.find())

    def disaster(self, idx):
        return self.mongo.db.disasters.find_one({'_id': idx})

    def people_in_danger(self, danger):
        # return self.mongo.db.checkin.find({'loc': {'$near': [danger['location']['lon'], danger['location']['lat']]}})
        return self.mongo.db.users.find()


class MongoEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, InsertOneResult) or isinstance(o, UpdateResult):
            return str(o)
        return json.JSONEncoder.default(self, o)


def mongo_dumps(obj):
    return json.dumps(obj, cls=MongoEncoder)
