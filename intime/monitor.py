from urllib.request import urlopen
from dateutil import parser
import datetime
import json


class DangerMonitor:
    demo_places = {
        'krakow': {
            'lat': 50.061701,
            'lon': 19.937418,
            'rad': 30/6371.0
        },
        'tauron': {
            'lat': 50.0676558,
            'lon': 19.988532,
            'rad': 2/6371.0
        }
    }

    def __init__(self):
        pass

    def grab_rwlabs(self):
        url = ('https://api.reliefweb.int/v1/disasters?fields[include][]='
               'primary_country&fields[include][]=current&fields[include][]='
               'primary_type&fields[include][]=date&limit=1000'
               )
        response = urlopen(url)
        data = json.loads(response.read().decode(
            response.info().get_content_charset('utf-8')))
        return self.transform_rwlabs(data)

    def transform_rwlabs(self, data):
        return [RwlabsEncoder.danger_from_rwlabs(entry)
                for entry in data['data']]

    def generate_current_danger(self, kind, place):
        return {
            'location': self.demo_places[place],
            'name': kind,
            'current': True,
            'timestamp': int(datetime.datetime.now().timestamp())
        }


class RwlabsEncoder:
    @classmethod
    def danger_from_rwlabs(cls, entry):
        fields = entry['fields']
        return {
            'location': {
                'lat': fields['primary_country']['location']['lat'],
                'lon': fields['primary_country']['location']['lon'],
                'rad': 100/6371.0
            },
            'name': fields['primary_type']['name'],
            'current': fields['current'],
            'timestamp': int(parser.parse(
                fields['date']['created']).timestamp())
        }
