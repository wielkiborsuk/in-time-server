from flask import Flask
from flask import request
from flask_socketio import SocketIO
from intime.notifier import Notifier
from intime.repository import Repo
from intime.repository import mongo_dumps
from intime.monitor import DangerMonitor
import json

app_name = 'intime'
app = Flask(app_name)
app.config['SECRET_KEY'] = 'my development secret ingredient!'
app.config['FCM_API_KEY'] = (
    'AAAAlTG0Bns:APA91bHJOUyIqmMY5yBQuxLe0Sl5-jtfFnCH_dd_DIFFk-'
    'A9I1vBKIzsGIt75dTT8ZM-IA0Zn2vF9NF6USn6rGHhilphEMHL3Ye1xXUT'
    'PqTJq_ZM09Salm4rgojV4iyGKl8vtu7XKKx5'
)
app.config['MONGO_DBNAME'] = app_name

fcm_notifier = Notifier(app)
repo = Repo(app)
monitor = DangerMonitor()
sio = SocketIO(app)


@app.route('/')
def hello():
    return 'hello'


@app.route('/data_init')
def data_init():
    repo.mongo.db.client.drop_database(app_name)
    user_a = {
        'email': 'a@a.com',
        'registration_id': '',
        'devices': [
            'fUfg-Fn3Jsw:APA91bE4Ynw6sVLxrLWlUwOXIXu7qprfpmuMRkGoRfpR0FW6IuvZg-'
            'z3TBbpGaN3LBxmTygqxswnWyEputWNKpBAZRqQyzvE7D-dBZSkn4vD-aq1e7-G4xwW'
            '_8AaWdm-DSdoJdMw7JK4'
        ],
        'observers': ['b@a.com']
    }
    user_b = {
        'email': 'b@a.com',
        'registration_id': '',
        'devices': [
            'cBbHwpjLRls:APA91bG-e4D4xDW7oymodV2ZpmnRV6gn-WUaNIldTTuV0nEmCBF2at'
            'R4uZv66l1zJ5etRjuRxp9vDZ6hzU1UMtiuLTn5p6oV9ARwYIJpQUkW2L7Hk53d4FBj'
            'UCRF8GsQ4J5M1tscBjJc'
        ],
        'observers': ['c@a.com', 'd@a.com']
    }

    repo.register(user_a)
    repo.register(user_b)
    return 'OK'


@app.route('/notify')
def sender_test():
    print(repo.users())
    print([u for u in repo.users()])
    ids = [device
           for user in repo.users()
           if user and 'devices' in user
           for device in user['devices']]
    print(ids)
    return json.dumps(fcm_notifier.send_fcm(
        ids, 'Are you OK?', 'Please verfiy that you are safe'))


@app.route('/read')
def reader_test():
    return fcm_notifier.get_firebase()


@app.route('/checkin', methods=['POST'])
def checkin():
    checkin_data = request.get_json()
    return mongo_dumps(repo.checkin_save(checkin_data))


@app.route('/checkin', methods=['GET'])
def checkin_list():
    return mongo_dumps(repo.checkin_all())


@app.route('/observe', methods=['POST'])
def observe():
    observe_request = request.get_json()
    return mongo_dumps(repo.add_observer(
        observe_request['observed'], observe_request['observer']))


@app.route('/register', methods=['POST'])
def register_device():
    return mongo_dumps(repo.register(request.get_json()))


@app.route('/users')
def users():
    return mongo_dumps(repo.users())


@app.route('/disasters/init')
def fetch_disasters_manually():
    disasters = monitor.grab_rwlabs()
    for d in disasters:
        repo.disaster_save(d)
    return mongo_dumps(repo.disasters())


@app.route('/disasters')
def disasters():
    return mongo_dumps(repo.disasters())


@app.route('/demo/trigger')
def trigger_danger():
    danger = monitor.generate_current_danger(
        request.args.get('type'), request.args.get('place'))
    idx = repo.disaster_save(danger).inserted_id
    danger = repo.disaster(idx)

    people_in_danger = repo.people_in_danger(danger)
    people = [{'email': p['email'], } for p in people_in_danger]

    repo.disaster_add_people(idx, people)

    for p in people_in_danger:
        for d in p['devices']:
            sio.emit('danger', {'danger_id': str(idx)}, room=d)

    sio.emit('danger', {'danger_id': str(idx)})
    sio.emit('checkArea', {'danger': mongo_dumps(danger)})

    return mongo_dumps(danger)


@sio.on('message')
def handle_message(message):
    print('generic message')
    print(message)


@sio.on('imSafe')
def handle_safe():
    # person = repo.find_user_by_device(request.sid)
    # repo.disaster_update_person(
    print('he is safe ' + str(request.sid))
    # emit('personInArea', repo.disaster_status(1))
    sio.emit('personInArea', {'danger': 'i'})
    sio.emit('personInAreaIsSafe')


@sio.on('needHelp')
def handle_help():
    # person = repo.find_user_by_device(request.sid)
    print('he needs help ' + str(request.sid))
    # emit('personInArea', repo.disaster_status(1))
    sio.emit('personInArea', {'danger': 'i'})
    sio.emit('personInAreaNeedsHelp')


@sio.on('register')
def handle_register(email):
    repo.register({'email': email, 'registration_id': request.sid})
