from pyfcm import FCMNotification

# from twisted.internet import reactor
# from txfcm import TXFCMNotification


class Notifier:
    def __init__(self, app):
        # self.fcm = TXFCMNotification(app.config['FCM_API_KEY'])
        self.fcm = FCMNotification(app.config['FCM_API_KEY'])

    def get_firebase(self):
        return self.firebase.get('/', None)

    def send_fcm(self, ids, title, msg):
        return self.fcm.notify_multiple_devices(ids, msg, title)
